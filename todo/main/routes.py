from flask import render_template,request,redirect,Blueprint
from todo.model import Tags,Todo,Comments
from todo import db
from flask_login import current_user,login_required
from datetime import datetime

main = Blueprint('main', __name__,template_folder='templates')

@main.route("/home",methods = ['POST', 'GET'])
@login_required
def home():
    ''' todo_table->description,is_done,deadline,created_at,updated_at,permission_id,user_id '''
    '''tags_table->tag ,user_id ,todo_id ,permission_id,created_at,updated_at'''
    '''Comments_table->id,body,user_id ,todo_id ,permission_id,created_at,updated_at'''
    if request.method == 'POST':
        title=request.form['title']
        tag=request.form['tag']
        comment = request.form['comment']
        if title!=None:
            entry1=Todo(description=title,is_done=False,deadline=datetime.utcnow(),updated_at=datetime.utcnow(),permission_id=1,user_id=current_user.id)
            entry2=Tags(tag=tag,user_id=current_user.id,permission_id=1,updated_at=datetime.utcnow())
            entry3=Comments(body=comment,user_id=current_user.id,permission_id=1,updated_at=datetime.utcnow())
            entry1.posts.append(entry2)
            entry1.postc.append(entry3)
            db.session.add(entry1)
            db.session.add(entry1)
            db.session.commit()
    mydata1 = Todo.query.all()
    mydata2=Tags.query.all()
    return render_template("bootstap_todo.html",alltodo=mydata1,alltag=mydata2)

@main.route("/delete/<int:sno>")
@login_required
def delete(sno):
    Tags.query.filter_by(todo_id=sno).delete()
    Comments.query.filter_by(todo_id=sno).delete()
    db.session.commit()

    todod = Todo.query.filter_by(id=sno).first()
    db.session.delete(todod)
    db.session.commit()
    return redirect('/home')

@main.route("/update/<int:sno>",methods = ['POST', 'GET'])
@login_required
def update(sno):
    if request.method == 'POST':
        title=request.form['title']
        is_done = bool(request.form['radio'])
        tag=request.form['tag']
        comment = request.form['comment']
        todod = Todo.query.filter_by(id=sno).first()
        tagd = Tags.query.filter_by(todo_id=sno).first()
        commentd=Comments.query.filter_by(todo_id=sno).first()

        commentd.body=comment
        todod.description =title
        todod.is_done=is_done
        todod.updated_at=tagd.updated_at=commentd.updated_at=datetime.utcnow()
        tagd.tag=tag
        todod.posts.append(tagd)
        todod.postc.append(commentd)
        db.session.add(todod)
        db.session.commit()
        return redirect('/home')
    mydata=Todo.query.filter_by(id=sno).first()
    mydata1= Tags.query.filter_by(todo_id=sno).first()
    mydata2 = Comments.query.filter_by(todo_id=sno).first()
    return render_template("update.html", alltodo=mydata,alltag=mydata1,allcomment=mydata2)

@main.route("/view/<int:sno>",methods = ['POST', 'GET'])
@login_required
def view(sno):
    mydata=Todo.query.filter_by(id=sno).first()
    mydata1= Tags.query.filter_by(todo_id=sno).all()
    mydata2 = Comments.query.filter_by(todo_id=sno).all()
    return render_template("view_all.html", alltodo=mydata,alltag=mydata1,allcomment=mydata2)

@main.route("/add/<int:sno>",methods = ['POST', 'GET'])
@login_required
def add(sno):
    if request.method == 'POST':
        tag = request.form.get('tag')
        comment = request.form.get('comment')

        # tag = request.form['tag']
        # comment = request.form['comment']
        if tag!=None:
            entry = Tags(tag=tag, todo_id=sno,user_id=current_user.id, permission_id=1, updated_at=datetime.utcnow())
            db.session.add(entry)
            db.session.commit()

        if comment!=None:
            entry_comment = Comments(body=comment,todo_id=sno, user_id=current_user.id, permission_id=1, updated_at=datetime.utcnow())
            db.session.add(entry_comment)
            db.session.commit()

    mydata=Todo.query.filter_by(id=sno).first()
    mydata1= Tags.query.filter_by(todo_id=sno).all()
    mydata2 = Comments.query.filter_by(todo_id=sno).all()
    return render_template("add.html", alltodo=mydata,alltag=mydata1,allcomment=mydata2)