from todo import db,login_manager
from datetime import datetime
from flask_login import UserMixin

@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(user_id)

class Users(db.Model,UserMixin):
    __tablename__='users'
    __table_args__={'schema':'relation'}
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(20), nullable=False)
    last_name = db.Column(db.String(20))
    username = db.Column(db.String(20),unique=True, nullable=False)
    email = db.Column(db.String(20), unique=True, nullable=False)
    user_type=db.Column(db.Boolean,nullable=False)
    password = db.Column(db.String(100), nullable=False)
    created_at = db.Column(db.DateTime,default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)
    # children = db.relationship("Usergp", backref="parent")

class Usergp(db.Model):
    __tablename__='user_gp'
    __table_args__={'schema':'relation'}
    username = db.Column(db.String(20), db.ForeignKey('relation.users.username'), nullable=False,primary_key=True)
    permission=db.Column(db.Integer, db.ForeignKey('relation.user_permissions.id'), nullable=False)
    group_id = db.Column(db.Integer, db.ForeignKey('relation.groups.id'), nullable=False)

class Userpermissions(db.Model):
    __tablename__='user_permissions'
    __table_args__={'schema':'relation'}
    id = db.Column(db.Integer, primary_key=True)
    can_view = db.Column(db.Boolean, default=True)
    can_add = db.Column(db.Boolean, default=True)
    can_edit = db.Column(db.Boolean, default=False)
    can_delete = db.Column(db.Boolean, default=False)



class Todo(db.Model):
    __tablename__ = 'todo'
    __table_args__ = {'schema': 'relation'}
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(75), nullable=False)
    is_done= db.Column(db.Boolean, nullable=False)
    deadline=db.Column(db.DateTime,default=datetime.utcnow)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime)
    permission_id= db.Column(db.Integer, db.ForeignKey('relation.user_permissions.id'),default=1)
    user_id = db.Column(db.Integer, db.ForeignKey('relation.users.id'))



class Tags(db.Model):
    __tablename__ = 'tags'
    __table_args__ = {'schema': 'relation'}
    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(30), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('relation.users.id'))
    todo_id = db.Column(db.Integer, db.ForeignKey('relation.todo.id'))
    permission_id = db.Column(db.Integer, db.ForeignKey('relation.user_permissions.id'))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime)
    category = db.relationship('Todo',backref=db.backref('posts', lazy=True))

class Comments(db.Model):
    __tablename__ = 'comments'
    __table_args__ = {'schema': 'relation'}
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('relation.users.id'))
    todo_id = db.Column(db.Integer, db.ForeignKey('relation.todo.id'))
    permission_id = db.Column(db.Integer, db.ForeignKey('relation.user_permissions.id'))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime,default=datetime.utcnow)
    category = db.relationship('Todo', backref=db.backref('postc', lazy=True))

class Groups(db.Model):
    __tablename__='groups'
    __table_args__={'schema':'relation'}
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), db.ForeignKey('relation.users.username'))
    super = db.Column(db.Boolean, default=False)
    normal = db.Column(db.Boolean, default=False)
    base = db.Column(db.Boolean, default=True)
    category = db.relationship('Users', backref=db.backref('postu', lazy=True))






