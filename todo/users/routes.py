from flask import render_template,request,redirect,flash,Blueprint
from todo.model import Users,Groups
from todo import db,bcrypt
from flask_login import login_user,logout_user,login_required
from datetime import date,datetime

users = Blueprint('users', __name__,template_folder='templates')


@users.route("/",methods=['GET','POST'])
def signup():
    if(request.method=='POST'):
        '''add entery to db'''
        ''''Groups_table->id,username,super,normal,base '''
        fname=request.form.get('fname')
        lname = request.form.get('lname')
        password = request.form.get('password')
        username = request.form.get('username')
        email = request.form.get('email')
        usertype=request.form.get('usertype')
        password=bcrypt.generate_password_hash(password).decode('utf-8')
        entry=Users(first_name=fname, last_name=lname, username=username, email=email, user_type=True,
             password=password)
        if(usertype=="2"):
            entry1=Groups(super=True,normal=False,base=False)
        elif(usertype=="1"):
            entry1=Groups(super=False,normal=True,base=False)
        else:
            entry1 = Groups(super=False, normal=False, base=True)
        entry.postu.append(entry1)


        db.session.add(entry)
        db.session.commit()
        return redirect("/")
    return render_template('login.html')
@users.route("/login",methods=['GET','POST'])
def login():
    if (request.method == 'POST'):
        '''add entery to db'''
        password = request.form.get('password')
        email = request.form.get('email')
        user=Users.query.filter_by(email=email).first()
        if user and bcrypt.check_password_hash(user.password,password):
            login_user(user)
            return redirect('home')
        else:
            flash('unsecessful','danger')
            return render_template('login.html',data='login unsucessfull')

@users.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect('/')